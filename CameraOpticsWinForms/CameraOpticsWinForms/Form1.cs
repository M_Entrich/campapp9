﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WebEye.Controls.WinForms.WebCameraControl;

namespace CameraOpticsWinForms
{
    public partial class Form1 : Form
    {
        List<WebCameraId> cameras = new List<WebCameraId>();

        Bitmap LastBitmap = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cameras = new List<WebCameraId>(webCameraControl1.GetVideoCaptureDevices());

            InitializeComboBox();
           // comboBox1.SelectedIndex = 0;
        }

        private void InitializeComboBox()
        {
            foreach (WebCameraId id in cameras)
            {
                comboBox1.Items.Add(id.Name);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0) return;

            webCameraControl1.StartCapture(cameras[comboBox1.SelectedIndex]);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (TraversenImg.Visible == false)
            {
                LastBitmap = webCameraControl1.GetCurrentImage();

                TraversenImg.Invalidate();


                button1.Text = "Bild Speichern";
                TraversenImg.Visible = true; // wieder sichtabres bild
            }
            else
            {

                button1.Text = "Bild erstellen";
                TraversenImg.Visible = false; // unsichtbar machen
            }

        }

        private void TraversenImg_Paint(object sender, PaintEventArgs e)
        {
            if (LastBitmap == null) return;

            e.Graphics.Clear(Color.Wheat);

            Image image = ResizeImage(LastBitmap, TraversenImg.Width, TraversenImg.Height);

            e.Graphics.DrawImage(image, new Point(0, 0));
        }

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
